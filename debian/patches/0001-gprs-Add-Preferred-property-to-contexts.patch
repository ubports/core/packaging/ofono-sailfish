From 161fbe40a2a7f14dfe9b46398003722c2da0d349 Mon Sep 17 00:00:00 2001
From: Marius Gripsgard <marius@ubports.com>
Date: Tue, 9 Jun 2020 00:18:22 +0200
Subject: [PATCH 3/8] gprs: Add Preferred property to contexts

Add Preferred property to org.ofono.ConnectionContext. This property
is a way to express that a context is preferred over the others for
activation. It is a facility used by the ofono clients, but it is not
actually used internally.
---
 ofono/src/gprs.c | 50 +++++++++++++++++++++++++++++++++++++++++++++++-
 1 file changed, 49 insertions(+), 1 deletion(-)

Index: ofono-sailfish/ofono/src/gprs.c
===================================================================
--- ofono-sailfish.orig/ofono/src/gprs.c
+++ ofono-sailfish/ofono/src/gprs.c
@@ -128,6 +128,7 @@ struct ofono_gprs_context {
 struct pri_context {
 	ofono_bool_t active;
 	enum ofono_gprs_context_type type;
+	gboolean preferred;
 	char name[MAX_CONTEXT_NAME_LENGTH + 1];
 	char message_proxy[MAX_MESSAGE_PROXY_LENGTH + 1];
 	char message_center[MAX_MESSAGE_CENTER_LENGTH + 1];
@@ -1055,7 +1056,7 @@ static void append_context_properties(st
 	const char *type = gprs_context_type_to_string(ctx->type);
 	const char *proto = gprs_proto_to_string(ctx->context.proto);
 	const char *name = ctx->name;
-	dbus_bool_t value;
+	dbus_bool_t value, preferred;
 	const char *strvalue;
 	struct context_settings *settings;
 	const char *interface;
@@ -1065,6 +1066,9 @@ static void append_context_properties(st
 	value = ctx->active;
 	ofono_dbus_dict_append(dict, "Active", DBUS_TYPE_BOOLEAN, &value);
 
+	preferred = ctx->preferred;
+	ofono_dbus_dict_append(dict, "Preferred", DBUS_TYPE_BOOLEAN, &preferred);
+
 	ofono_dbus_dict_append(dict, "Type", DBUS_TYPE_STRING, &type);
 
 	ofono_dbus_dict_append(dict, "Protocol", DBUS_TYPE_STRING, &proto);
@@ -1208,6 +1212,33 @@ static void pri_deactivate_callback(cons
 	}
 }
 
+static DBusMessage *pri_set_preferred(struct pri_context *ctx,
+					DBusConnection *conn,
+					DBusMessage *msg, gboolean preferred)
+{
+	GKeyFile *settings = ctx->gprs->settings;
+
+	if (ctx->preferred == preferred)
+		return dbus_message_new_method_return(msg);
+
+	ctx->preferred = preferred;
+
+	if (settings) {
+		g_key_file_set_boolean(settings, ctx->key, "Preferred",
+								preferred);
+		storage_sync(ctx->gprs->imsi, SETTINGS_STORE, settings);
+	}
+
+	g_dbus_send_reply(conn, msg, DBUS_TYPE_INVALID);
+
+	ofono_dbus_signal_property_changed(conn, ctx->path,
+					OFONO_CONNECTION_CONTEXT_INTERFACE,
+					"Preferred", DBUS_TYPE_BOOLEAN,
+					&preferred);
+
+	return NULL;
+}
+
 static void gprs_set_attached_property(struct ofono_gprs *gprs,
 					ofono_bool_t attached)
 {
@@ -1654,6 +1685,16 @@ static DBusMessage *pri_set_property(DBu
 		return NULL;
 	}
 
+
+	if (!strcmp(property, "Preferred")) {
+		if (dbus_message_iter_get_arg_type(&var) != DBUS_TYPE_BOOLEAN)
+			return __ofono_error_invalid_args(msg);
+
+		dbus_message_iter_get_basic(&var, &value);
+
+		return pri_set_preferred(ctx, conn, msg, value);
+	}
+
 	/* All other properties are read-only when context is active */
 	if (ctx->active == TRUE)
 		return __ofono_error_in_use(msg);
@@ -2288,6 +2329,8 @@ static void write_context_settings(struc
 				gprs_context_type_to_string(context->type));
 	g_key_file_set_string(gprs->settings, context->key, "Protocol",
 				gprs_proto_to_string(context->context.proto));
+	g_key_file_set_boolean(gprs->settings, context->key, "Preferred",
+				context->preferred);
 
 	if (context->type == OFONO_GPRS_CONTEXT_TYPE_MMS) {
 		g_key_file_set_string(gprs->settings, context->key,
@@ -3668,6 +3711,7 @@ static gboolean load_context(struct ofon
 	char *authstr = NULL;
 	gboolean ret = FALSE;
 	gboolean legacy = FALSE;
+	gboolean preferred;
 	struct pri_context *context;
 	enum ofono_gprs_context_type type;
 	enum ofono_gprs_proto proto;
@@ -3703,6 +3747,9 @@ static gboolean load_context(struct ofon
 	if (gprs_proto_from_string(protostr, &proto) == FALSE)
 		goto error;
 
+	preferred = g_key_file_get_boolean(gprs->settings, group,
+							"Preferred", NULL);
+
 	username = g_key_file_get_string(gprs->settings, group,
 						"Username", NULL);
 	if (username == NULL)
@@ -3757,6 +3804,7 @@ static gboolean load_context(struct ofon
 	strcpy(context->context.password, password);
 	strcpy(context->context.apn, apn);
 	context->context.proto = proto;
+	context->preferred = preferred;
 	context->context.auth_method = auth;
 
 	if (msgproxy != NULL)
